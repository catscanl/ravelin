<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

// define('DB_NAME', 'wordpress_ravelin');
define('DB_NAME', 'ravelinWPDB');

/** MySQL database username */
// define('DB_USER', 'root');
define('DB_USER', 'ravelin_wp_user');

/** MySQL database password */
// define('DB_PASSWORD', 'root');
define('DB_PASSWORD', 'u0#w*d0NVQc!q5jqUQ1O');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P|ec0uQC-Hz5zAdZc{[ O>6AbX|rK!IK! c%>V00nfDAKZU,+qD+*BcPj]zEZq#~');
define('SECURE_AUTH_KEY',  'Hye4M5a`$;u0F:aSBgux:n2udDxU^e0i%(G.${8%+L&_UNt59basrQ@MfKhv(bhp');
define('LOGGED_IN_KEY',    'Ut5c0GI^2-*A|L-hs7-_9- .NyaHelyvQfI+}uYk;v[ET#pP5X *B7]%Z@T|o{Qq');
define('NONCE_KEY',        '6.FP<#$U[;k|Yj1.Qu1tCivD!zu{bR:LwqwA2w{@)Z/ CW}7Nb|0XI{sxaf}q `4');
define('AUTH_SALT',        'Tr;VW#rj-Ql%KE?:nqwr1:<^q|EJl9=+|`7luv!01D?O}zD3Fs4wx%A=&3IT~|pK');
define('SECURE_AUTH_SALT', 'iws.i#A^gnLlZ-+MFn|n,s1-78^+!/B8Ih{0WA>O9>q4^(.tC-38TnigO^Im|=z>');
define('LOGGED_IN_SALT',   's,!1uGm|`?%<1OfHvLFN3#b1N=-71j+P$dju{O9q0m(:.!P(Po,TIp$/e%lU].S6');
define('NONCE_SALT',       ';gd]+*8R-.[:Rw1s]G<{P$oEt~f{QamE^MnMOG}mFK8lt!+RW/EEY?$ZGsE:M$!_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

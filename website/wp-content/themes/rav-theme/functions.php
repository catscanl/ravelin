<?php 

add_theme_support( 'post-thumbnails' ); 

function wpbootstrap_scripts_with_jquery() 
{ 
// Register the script like this for a theme: 
	wp_register_script( 'custom-script', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ) ); 
// For either a plugin or a theme, you can then enqueue the script: 
	wp_enqueue_script( 'custom-script' ); 
} 

add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' ); 

//register menu 
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );


//allow svg through media uploader
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//Read More Button For Excerpt
function my_more_link($more_link, $more_link_text) {
	return str_replace($more_link_text, 'Read more', $more_link);
}
add_filter('the_content_more_link', 'my_more_link', 10, 2);

add_action( 'widgets_init', 'child_register_sidebar' );


if ( function_exists('register_sidebar') )
register_sidebar(array(
'before_widget' => '<li id="%1$s" class="widget %2$s">', 'after_widget' => '</li>',
'before_title' => '<h2 class="widgettitle">',
'after_title' => '</h2>',
));

function wpb_postsbycategory() {
    // a query
    $the_query = new WP_Query( array( 'category_name' => 'popular', 'posts_per_page' => 5 ) ); 
    $string = "";
    // The Loop
    if ( $the_query->have_posts() ) {
        $string .= '<ul class="postsbycategory widget_recent_entries">';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            if ( has_post_thumbnail() ) {
                $string .= '<li>';
                $string .= '<a href="' . get_the_permalink() . '"rel="bookmark">' . 
                get_the_post_thumbnail($post_id, array( 150, 150) ) . 
                '<div class="populartext"><ul><li class="colourtitle">' . 
                get_the_title() . '</li><li>' . get_the_author()  . 
                '</li><li>'  . get_the_date() . '</li><li>'  . 
                '</li></ul></div></a></li><div class="break"></div>';
            } else { 
            // if no featured picture is found
                $string .= '<li>';
                $string .= '<a href="' . get_the_permalink() . '"rel="bookmark">' . 
                '<div class="populartext"><ul><li class="colourtitle">' . 
                get_the_title() . '</li><li>' . get_the_author()  . 
                '</li><li>'  . get_the_date() . '</li><li>'  . 
                '</li></ul></div></a></li><div class="break"></div>';
            }
        }
    } else {
        // no posts found
    }
    $string .= '</ul>';

    return $string;

    /* Restore strange Post Data */
    wp_reset_postdata();
}
// Add a shortcode
add_shortcode('categoryposts', 'wpb_postsbycategory');

// Enable shortcodes in calm widgets
add_filter('widget_text', 'do_shortcode');
?>
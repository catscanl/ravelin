<?php /* Template Name: Terms Page */ ?>

<?php get_header(); ?>

<div class="terms-wrap">
	<div class="section-wrap container-fluid">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
				<?php the_content(); ?> 
			<?php endwhile; else: ?> 
				<p><?php _e('Sorry, this page does not exist.'); ?></p> 
			<?php endif; ?>

	</div>
</div>

<?php get_footer(); ?>
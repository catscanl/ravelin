
<div class="sidebarblog">

	<div class="sidebar-popular">
		<h2>Most Popular</h2>
		<p><?php echo wpb_postsbycategory() ?></p>
	</div>

	<div class="sidebar-newsletter">
		<h2>Newsletter</h2>

	    <p>Sign up to our Newsletter and receive monthly news updates</p>
		<form action="//ravelin.us10.list-manage.com/subscribe/post?u=cb42ed1c9372025aa67696829&amp;id=4cc3c21f7a" 
	method="post" id="mc-embedded-subscribe-form" target="_blank">
	    	<div class="email-submit">
	        	<div class="email2">
	            	<input type="email" class="email-cust form-control" name="EMAIL" required="required" placeholder="Email Address">
	        	</div>
	        	<div style="position: absolute; left: -5000px;">
	            	<input type="text" name="b_cb42ed1c9372025aa67696829_4cc3c21f7a" tabindex="-1" value="">
	        	</div>
	        	<div class="submit2">
	            	<button type="submit" class="submit-cust btn btn-primary">Submit</button>
	        	</div>
	    	</div>
		</form>
	</div>

	<div class="sidebar-categories">
		<h2>Categories</h2>
		<ul class="nav nav-pills nav-stacked">
			<p><?php wp_list_categories('exclude=-6,-7,-8orderby=name&title_li='); ?></p>
		</ul>
	</div>



</div>


<?php /* Template Name: Contact Page */ 
?>


<?php include 'header.php';?>

<div class="contact-main">
    <div class="section-wrap container-fluid">
        <div class="row-fluid">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="contact-image">
                    <?php the_post_thumbnail('full'); ?>
                </div>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 

                <?php the_content(); ?> 

              <?php endwhile; else: ?> 

                <p><?php _e('Sorry, this page does not exist.'); ?></p> 

              <?php endif; ?> 

            </div> 
        </div>   
    </div>

    <div class="section-wrap container-fluid">
        <div class="row">
            <form class="form-horizontal" method="post">
                <fieldset>
            <?php if(get_field('contact-form')){ 
                echo  get_field('contact-form') ;
             } ?> 
                </fieldset>
            </form>
        </div>
    </div>
</div>




<?php include 'footer.php';?>
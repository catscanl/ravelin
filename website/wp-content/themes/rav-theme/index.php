<?php get_header(); ?>

<div class="contact-main" style="margin-top:150px; padding-bottom:400px;">
    <div class="section-wrap container-fluid">
        <div class="row-fluid">
            <div class="col-sm-8 col-sm-offset-2">
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 

                <?php the_content(); ?> 

              <?php endwhile; else: ?> 

                <p><?php _e('Sorry, this page does not exist.'); ?></p> 

              <?php endif; ?> 

            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
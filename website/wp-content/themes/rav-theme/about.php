<?php /* Template Name: About Page */ ?>
<?php get_header(); ?>

<div class="about-section-top">
	<div class="section-wrap container-fluid">
		<div class="row">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 

				<?php the_content(); ?> 

			<?php endwhile; ?>

			<?php endif; ?> 

		</div>
	</div>
</div>

<div class="about-section-1">
  	<div class="section-wrap container-fluid">
    	<div class="row" id="about-1">
    		<?php if(get_field('about-1')){ 
    			echo '<p>' . get_field('about-1') . '</p>';
    		 } ?> 
    	</div>
    </div>
</div>

<div class="about-section-2">
  	<div class="section-wrap container-fluid">
    	<div class="row" id="about-2">
    		<?php if(get_field('about-2')){ 
    			echo '<p>' . get_field('about-2') . '</p>';
    		 } ?> 
    	</div>
    </div>
</div>

<div class="about-section-1">
  	<div class="section-wrap container-fluid">
    	<div class="row" id="about-3">
    		<?php if(get_field('about-3')){ 
    			echo '<p>' . get_field('about-3') . '</p>';
    		 } ?> 
    	</div>
    </div>
</div>

<div class="about-section-2">
  	<div class="section-wrap container-fluid">
    	<div class="row" id="about-4">
    		<?php if(get_field('about-4')){ 
    			echo '<p>' . get_field('about-4') . '</p>';
    		 } ?> 
    	</div>
    </div>
</div>

<div class="about-section-1">
  	<div class="section-wrap container-fluid">
    	<div class="row" id="about-5">
    		<?php if(get_field('about-5')){ 
    			echo '<p>' . get_field('about-5') . '</p>';
    		 } ?> 
    	</div>

		<div class="row">
      		<div class="col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">
        		<a href="/contact/" type="button" class="btn btn-primary btn-lg color1  btn-block">Ask us a question?</a>
      		</div>
    	</div>

	</div>
</div>

<?php get_footer(); ?>
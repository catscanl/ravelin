
         <!-- FOOTER -->
        <footer>
            <div class="footer">
                <div class="section-wrap container">
                    <div class="textarea">
                        <div class="row-fluid">


                            <div class="col-sm-3">
                                <h3>Get in touch</h3>
                                <p>White Bear Yard</p>
                                <p>144a Clerkenwell Road</p>
                                <p>London EC1R 5DF</p>
                                <a href="https://twitter.com/ravelinhq" target="_blank"> <img class="" src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.svg" alt="lightbulb" style="width:40px;"></a>
                            </div>


                            <div class="col-sm-3">
                                <h3>Legal</h3>
                                <p><a href="/terms/">Terms and Conditions</a></p>
                                <p><a href="/privacy/">Privacy Policy</a></p>
                            </div>

                            <div class="col-sm-5 col-sm-offset-1">
                                <h3>Sign Up</h3>
                                <p>Find out how Ravelin can help you tackle fraud</p>
                                <form action="//ravelin.us10.list-manage.com/subscribe/post?u=cb42ed1c9372025aa67696829&amp;id=4cc3c21f7a" 
                              method="post" id="mc-embedded-subscribe-form" target="_blank">
                                    <div class="email-submit">
                                        <div class="email1">
                                            <input type="email" class="email-cust form-control" name="EMAIL" required="required" placeholder="Email Address">
                                        </div>
                                        <div style="position: absolute; left: -5000px;">
                                            <input type="text" name="b_cb42ed1c9372025aa67696829_4cc3c21f7a" tabindex="-1" value="">
                                        </div>
                                        <div class="submit1">
                                            <button type="submit" class="submit-cust btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div> 

                        </div> <!-- .row -->
                    </div> <!-- textarea -->
                </div> <!-- .container-fluid -->
            </div> <!-- footer -->
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <?php wp_footer(); ?>
        
    </body>
</html>
<?php get_header(); ?>

<div class="blog-wrap">
  <div class="section-wrap container-fluid">
      <div class="row">
          <div class="col-sm-7">
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 

                <h1><?php the_title(); ?></h1> 
                <?php the_post_thumbnail(); ?>
                <p><?php the_content(); ?></p>
                

                <div class="navigation">
                  <div class="pull-left">
                    <p><?php previous_post_link('&laquo; %link', '%title', FALSE, '6,7,8'); ?></p>
                    </div>
                  <div class="pull-right">
                    <p><?php next_post_link('%link &raquo;', '%title', FALSE, '6,7,8'); ?></p>
                  </div> 
                </div>
              
              <div class="comment-cust">
                <p><?php comments_template(); ?></p>
              </div>

              <?php endwhile; else: ?> 

                <p><?php _e('Sorry, this page does not exist.'); ?></p> 

              <?php endif; ?> 
          </div> 

          <div class="col-sm-5">
                  <?php get_sidebar(); ?>
          </div>

      </div>
  </div>
</div>

<?php get_footer(); ?>
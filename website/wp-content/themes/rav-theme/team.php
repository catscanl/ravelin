<?php /* Template Name: Team Page */ ?>

<?php get_header(); ?>

<div class="team-main contact-main">

    <div class="team-top">
        <div class="section-wrap container-fluid">
            <div class="row-fluid">
                <div class="col-sm-8 col-sm-offset-2">
                  
                  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 

                    <?php the_content(); ?> 

                  <?php endwhile; ?> 
                  <?php endif; ?> 

                </div>
            </div>
        </div>
    </div>

    <div class="section-wrap container-fluid">
        <div class="row" style="clear:both;">
            <?php
            query_posts('category_name=team&orderby=date&order=ASC');
            $counter = 1;

            if ( have_posts() ) : while ( have_posts() ) : the_post();
                echo '<div class="col-md-4 team-item-wrap">';

                if ( has_post_thumbnail() ) { 
                        echo '<div class="img-responsive">';
                            the_post_thumbnail();
                        echo '</div>';
                    } 
                    
                        echo '<span class="team-head">';
                        the_title();
                        echo '</span>';
                        the_content();
                    
                echo '</div>';

                if ($counter % 3 == 0) {
                    echo '</div><div class="row" style="clear:both;">';
                }

                $counter++;

            endwhile; endif;

            wp_reset_query();?>
        </div>
    </div>

</div>

<?php get_footer(); ?>
<?php /* Template Name: Ravelin Blog */ ?>

<?php get_header(); ?>

<div class="blog-wrap">
    <div class="section-wrap container-fluid">
        <div class="row">
            <div class="col-sm-7">
                <?php query_posts('cat=-8,-7,-6'); ?>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
                  <div class="post-cust">
                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    <?php the_post_thumbnail(); ?>
                    <?php the_content(); ?> 

                    <p><small><?php the_time('F jS Y'); ?> by <?php the_author(); ?></small></p>
                    </div>
                <hr>

                <?php endwhile; else: ?> 

                  <p><?php _e('Sorry, there are no posts.'); ?></p> 

                <?php endif; ?> 
            </div> 

            <div class="col-sm-5">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>
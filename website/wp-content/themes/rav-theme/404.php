
<?php /* Template Name: Ravelin Blog */ ?>

<?php get_header(); ?>

<div class="blog-wrap">
    <div class="section-wrap container-fluid">
        <div class="row">
            <div class="col-sm-7">
            	<h1>404 ‐ page not found</h1>
				<p>Sorry, the page you’re looking for has gotten lost!</p>
            </div> 
        </div>
    </div>
</div>

<?php get_footer(); ?>
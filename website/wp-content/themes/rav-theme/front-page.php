<?php /* Template Name: Front Page */ ?>

<?php get_header('front'); ?>

<div class="jumbotron">
	<div class="section-wrap container-fluid">
		<div class="row">
			<!-- <div class="col-md-1"></div> -->
			<div class="col-sm-6 col-sm-offset-3">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
                	<?php the_content(); ?> 
              	<?php endwhile;?>           
              	<?php endif; ?> 
				<form action="//ravelin.us10.list-manage.com/subscribe/post?u=cb42ed1c9372025aa67696829&amp;id=4cc3c21f7a" method="post" id="mc-embedded-subscribe-form" target="_blank">
					<div class="email-submit">
						<div class="email1">
							<input type="email" class="email-cust form-control" name="EMAIL" required="required" placeholder="Email Address">
						</div>
						<div style="position: absolute; left: -5000px;">
							<input type="text" name="b_cb42ed1c9372025aa67696829_4cc3c21f7a" tabindex="-1" value="">
						</div>
						<div class="submit1">
							<button type="submit" class="submit-cust btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="marketing-wrapper">
	<div class="section-wrap container-fluid marketing">
	<h1>Why Ravelin?</h1>
	<!-- Three columns of text below the carousel -->
		<div class="row">
			<?php
			query_posts('category_name=front-why&orderby=date&order=ASC');

			if ( have_posts() ) : while ( have_posts() ) : the_post();

				echo '<div class="col-sm-4">';
					the_post_thumbnail('full');
					echo '<h2>'; 
					the_title(); 
					echo '</h2>';
					the_content();
				echo '</div>';

			endwhile; endif;

			wp_reset_query();

			?>
		</div><!-- /.row -->
	</div>
</div>


<div class="marketing-wrapper2">
  	<div class="section-wrap container-fluid marketing">
    	<h1>What are the benefits?</h1>
	    <div class="row">

	    	<?php
			query_posts('category_name=front-benefits&orderby=date&order=ASC');
			$counter = 1;

			if ( have_posts() ) : while ( have_posts() ) : the_post();

				echo '<div class="col-sm-6">';

					if ( has_post_thumbnail() ) { 
						echo '<div class="pull-left">';
							the_post_thumbnail();
						echo '</div>';
					} 
					echo '<div class="benefits-body">';
						echo '<p><span class="benefits-head">';
						the_title();
						echo '</span></p>';
						echo '<p>';
						the_content();
						echo '</p>';
					echo '</div>';
				echo '</div>';

				if ($counter % 2 == 0) {
					echo '</div><div class="row">';
				}

				$counter++;

			endwhile; endif;

			wp_reset_query();

			?>

	    </div>
	    <div class="row">
         	<div class="col-md-4 col-md-offset-4">
            	<a href="/about/" type="button" class="btn btn-primary btn-lg color1 btn-block">Show me how it works</a>
          	</div>
        </div>
	</div>
</div>

<?php get_footer(); ?>